# backupninja attributes

default['backupninja']['admingroup'] = 'root'
default['backupninja']['configdirectory'] = '/etc/backup.d'
default['backupninja']['configs'] = nil
default['backupninja']['crontime'] = '0 * * * *'
default['backupninja']['logfile'] = '/var/log/backupninja.log'
default['backupninja']['loglevel'] = 4
default['backupninja']['logrotate'] = ['rotate 6', 'monthly', 'compress', 'missingok']
default['backupninja']['reportdirectory'] = '/var/lib/backupninja/reports'
default['backupninja']['reportemail'] = 'root'
default['backupninja']['reporthost'] = nil
default['backupninja']['reportinfo'] = false
default['backupninja']['reportspace'] = false
default['backupninja']['reportsuccess'] = true
default['backupninja']['reportuser'] = 'ninja'
default['backupninja']['reportwarning'] = true
default['backupninja']['usecolors'] = true
default['backupninja']['vservers'] = false
default['backupninja']['when'] = 'everyday at 01:00'
