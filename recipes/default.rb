#
# Cookbook:: backupninja
# Recipe:: default
#
# Copyright:: 2021, GSI Helmholtzzentrum für Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

confdir = node['backupninja']['configdirectory']
configs = node['backupninja']['configs'] || {}
possible_types = %w(dup ldap maildir makecd mysql pgsql rdiff)

package 'backupninja'

template '/etc/backupninja.conf' do
  source 'backupninja.conf.erb'
  variables node['backupninja']
  owner 'root'
  group 'root'
  mode '644'
end

unless node['backupninja']['logrotate'].nil?
  template '/etc/logrotate.d/backupninja' do
    source 'logrotate.erb'
    variables options: node['backupninja']['logrotate']
    owner 'root'
    group 'root'
    mode '644'
    action :delete if node['backupninja']['logrotate'].eql? false
  end
end

unless node['backupninja']['crontime'].nil?
  template '/etc/cron.d/backupninja' do
    source 'cron.erb'
    variables time: node['backupninja']['crontime']
    owner 'root'
    group 'root'
    mode '644'
    action :delete if node['backupninja']['crontime'].eql? false
  end
end

directory confdir do
  owner 'root'
  group node['backupninja']['admingroup']
  mode '2770'
end

# remove unwanted configuration files
::Dir["#{confdir}/*"].each do |filename|
  next if configs.include? ::File.basename(filename)

  file filename do
    action :delete
  end
end

# create/update configuration files
configs.each do |name, attrs|
  type = ::File.extname(name).sub(/^\./, '')
  if possible_types.include? type
    template "#{confdir}/#{name}" do
      source "#{type}.erb"
      variables attrs
      owner 'root'
      group node['backupninja']['admingroup']
      mode '660'
    end
  else
    Chef::Log.warn "Invalid type in '#{name}' for backupninja configuration"
  end
end
