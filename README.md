# backupninja Cookbook

This cookbook installs and configures [backupninja](https://github.com/sol1/backupninja).

## Requirements

Currently only Debian is supported.

## Recipes

`default`: Install backupninja and update all configuration files

## Attributes

backupninja's attributes can be found under `node['backupninja']`. The names are equal to the variable names in `backupninja.conf`. The default values reflect the ones found in a vanilla installation.

Additionally there are three attributes:

* `configs`: A hash containing attributes for configuration files (see below)
* `crontime`: The time declaration for backupninja's cron job; Set this to `nil` to ignore the cron job or `false` to remove it
* `logrotate`: An array of all logrotate options for backupninja's log file

## Configuration files

Additional configuration files are managed under `node['backupninja']['configs']`. The keys are the filenames and the values hashes with options.

E.g. a configuration file for MySQL can be configured like this:

```ruby
{
  backupninja: {
    configs: {
      '10-localhost.mysql': {
        databases: 'all',
        dbusername: 'backupninja',
        dbpassword: 'secretpass',
      },
    },
  },
}
```

The type of configuration file is derived from the filename's extension. Currently available are `dup`, `ldap`, `makecd`, `mysql`, `pgsql` and `rdiff`.
